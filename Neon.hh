<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\neon
{
	use Nette\Neon\Neon as NeonParser;
	use nuclio\plugin\fileSystem\reader\FileReader;
	use nuclio\plugin\format\driver\common\CommonMapInterface;


	<<provides('format::neon')>>
	class Neon implements CommonMapInterface
	{
		/**
		 * Convert neon format string to the PHP array
		 * @param  string $file 		String that need to be converted
		 * @return Map<string,mixed>    Data that already converted.
		 */
		public static function decode(string $filePath):Map<string,mixed>
		{
			$reader=FileReader::getInstance('fileSystem::local-disk',$filePath);
			$content=$reader->read();
			return new Map(NeonParser::decode($content));
		}

		public function read(string $filename, Map<arraykey,mixed> $options=Map{}):Map<string,mixed>
		{
			return Neon::decode($filename);
		}
		
		/**
		 * Convert PHP array format string to the NEON string
		 * @param  mixed  $file 		Data that want to be converted.
		 * @return Map<string,mixed>    Neon formatted string.
		 */
		public static function encode(mixed $file):string
		{
			return NeonParser::encode($file);
		}

		/**
		 * Convert PHP array format string to the NEON string and write it to disk.
		 * 
		 * @param  mixed  $filename		Data that want to be converted.
		 * @return Map<string,mixed>    Neon formatted string.
		 */
		public function write(string $filename, Vector<mixed> $data, Map<arraykey,mixed> $options=Map{}):bool
		{
			$NeonString=NeonParser::encode($filename);
			return (bool)file_put_contents($filename,$NeonString);
		}
	}
}
