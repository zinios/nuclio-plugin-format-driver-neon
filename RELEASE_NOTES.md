Release Notes
-------------
1.1.1
-----
* Fixed use of wrong interface.

1.1.0
-----
* Added write method which makes this driver compatible with the new CommonInterface.

1.0.1
-----
* Fixed dependency.

1.0.0
-----
* Initial Release.